#include <limits.h>
#include <string>
#include "Fraction.h"
#include <stdlib.h>

int nfailed = 0;
const bool FATALFAILURE = 1;

#define CHECK_PASS(pass) check_pass(pass, __FUNCTION__, __LINE__)

void check_pass(bool pass, const char * func, int line)
{
  if(!pass)
  {
    nfailed++;
    std::cout << "Failed at " << func << ":" << line << "\n";
    if(FATALFAILURE)
      abort();
  }
}

void test_operators(int a_n, int a_d, int b_n, int b_d,
		  Fraction  result_add, Fraction  result_subtract,
		  Fraction  result_multiply, Fraction  result_divide)
{
  Fraction a(a_n, a_d);
  Fraction b(b_n, b_d);
  Fraction c;
   
  double da = (double)a_n / (double)a_d;
  double db = (double)b_n / (double)b_d;
    
  bool val;

  std::cout << "\nTesting " << a_n << "/" << a_d << " and " << b_n << "/" << b_d << "\n";

  val = (a == a);
  std::cout << a << " == " << a <<  " is " << ( val ? "true" : "false")  << "\n";
  CHECK_PASS(val == true);

  val = (a == b);
  std::cout << a << " == " << b <<  " is " << ( val ? "true" : "false")  << "\n";
  CHECK_PASS(val == (da == db));

  val = (a != b);
  std::cout << a << " != " << b <<  " is " << ( val ? "true" : "false") << "\n";
  CHECK_PASS(val == (da != db));

  val = (a < b);
  std::cout << a << " < " << b <<  " is " << ( val ? "true" : "false") << "\n";
  CHECK_PASS(val == (da < db));

  val = (a > b);
  std::cout << a << " > " << b <<  " is " << ( val ? "true" : "false") << "\n";
  CHECK_PASS(val == (da > db));

  c = a + b;
  std::cout << a << " + " << b << " = " << c << "\n";
  CHECK_PASS(c == result_add);
   
  c = a - b;
  std::cout << a << " - " << b << " = " << c << "\n";
  CHECK_PASS(c == result_subtract);

  c = a * b;
  std::cout << a << " * " << b << " = " << c << "\n";
  CHECK_PASS(c == result_multiply);

  try {
     c = a / b;
     std::cout << a << " / " << b << " = " << c << "\n";
     CHECK_PASS(c == result_divide);
   }
   catch(std::exception & e)
   {
     std::cout << "Exception: " << e.what() << "\n";
   }
   
   std::cout << "\n";
}
int main()
{
  Fraction a;
  Fraction b;

  std::cout << "Test creating invalid object\n";
  try {
    a = Fraction (1, 0);
    //shouldn't get to this point
    nfailed++;
  }
  catch(std::exception & e)
  {
    std::cout << "Exception: " << e.what() << "\n";
  }
   
  test_operators(1, 2, 3, 4,
		Fraction(5, 4), Fraction(-1, 4), Fraction(3, 8), Fraction(2, 3));
  
  test_operators(-1, 2, 5, 7,
		Fraction(3, 14), Fraction(-17, 14), Fraction(-5, 14), Fraction(-7, 10));
  
  test_operators(-1, 2, 5, -7,
		Fraction(-17,14), Fraction(3,14), Fraction(5, 14), Fraction(7, 10));
  
  test_operators(0, 2, 5, -7,
		Fraction(-5, 7), Fraction(5, 7), Fraction(0, 1), Fraction(0, 1));
  
  /*
    last value is bogus because divide by 0 is not allowed.  If we reach the check for divide, 
    then it should fail
  */
  test_operators(1, 2, 0, -7, 
		Fraction(1, 2), Fraction(1, 2), Fraction(0, 1), Fraction(INT_MAX, INT_MAX));

  std::cout << nfailed << " tests failed\n";
  return 0;
}

