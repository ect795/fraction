#ifndef _FRACTION_H_
#define _FRACTION_H_

#include <iostream>
#include <stdexcept>

/*
 * Simple Fraction class that handles addition/subtraction,
 * multiplication/division, equality/non-equality,
 * greater than/less than, and display. For the sake of
 * simplicity, this doesn't check for overflow.
 */

class Fraction
{
 public:
  Fraction(int n, int d)  
  {
    if(d == 0)
    {
      throw std::invalid_argument("Denominator can't be zero");
    }
        
    _num = n;
    _denom = d;
     fixup();
  };

  Fraction () {};

  Fraction operator + (const Fraction & a ) const 
  {
    Fraction c;

    c._denom = _denom * a._denom;
    c._num = _num * a._denom + a._num * _denom;
    c.reduce();
    c.fixup();
    return c;
  };

  Fraction operator - (const Fraction & a) const
  {
    Fraction c = a;

    c._num = -c._num;
    c = *this + c;
    return c;

  };

  Fraction operator * (const Fraction & a) const
  {
    Fraction c;

    c._num = _num * a._num;
    c._denom = _denom * a._denom;
    c.reduce();
    c.fixup();
    return c;
  };

  Fraction operator / (const Fraction & a) const
  {
    if(a._num == 0)
    {
      throw std::invalid_argument("Can't divide by 0");
    }

    Fraction inverse(a._denom, a._num);

    return *this * inverse;
  };

  bool operator == (const Fraction & a) const
  {
    return (_num * a._denom) == (a._num * _denom); 
  };

  bool operator != (const Fraction & a) const
  {
    return !(*this == a);
  };

  bool operator < (const Fraction & a) const
  {
    return (_num * a._denom) < (a._num * _denom); 
  };

  bool operator > (const Fraction & a) const
  {
    return (_num * a._denom) > (a._num * _denom); 
  };

  friend std::ostream & operator << (std::ostream & stream, const Fraction & a) 
  {
    stream << a._num << "/" << a._denom;
    return stream;
  }
  
 private:
     /*numerator*/
     int _num;      
     /*denominator*/
     int _denom;    

     int gcd ( int a, int b)
     {
       int c;
       while (a != 0 )
       {
	 c = a; 
	 a = b % a; 
	 b = c;
       }
       return b;
     };


     /*
       Fixes up side effect from an operation. Should be
       called after any operation that creates/modifies an Fraction
     */
     void fixup ()
     {
       /*
	 In order to make sure that we're not
	 cancelling signedness when we shouldn't be,
	 make sure that the signedness is always in the
	 numerator.
       */
       if(_denom < 0)
       {
	 _denom = -_denom;
	 _num = -_num;
       }
     };

     void reduce ()
     {
       int div = gcd(_num, _denom);
       _num /= div;
       _denom /= div;
     };
};

#endif
